<footer class="footer">
	<div class="container">	
		<p class="pull-right">{{trans('menu.dibuat')}} <a href="#">BrillianISay</a></p>
		<p>&copy; {{date("Y")}} Washiindonesia. &middot; <a href="#">{{trans('menu.privasi')}}</a> &middot; <a href="#">{{trans('menu.kondisi')}}</a> &middot; <a href="#">{{trans('menu.kembali')}}</a> </p>
	</div>
</footer>
  <!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<script src="assets/bootstrap/dist/js/holder.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="assets/bootstrap/dist/js/ie10-viewport-bug-workaround.js"></script>