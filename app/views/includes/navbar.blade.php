<?php 
    $nama = Session::get('nama'); 
    $status = Session::get('status');
?>
<nav style="background-color:white" class="navbar navbar-default navbar-static-top" role="navigation">
  <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" >
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{ URL::to('') }}"><img src="{{ URL::to('assets/images/Logo.png')}}" height="27px" alt="washiindonesia"></a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="{{ URL::to('/')}}"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Beranda</a></li>
            @if ($status == '0')
            <li><a href="{{ URL::to('/gudang')}}"><span class="glyphicon glyphicon-upload" aria-hidden="true"></span> Gudang</a></li>
            <li><a href="{{ URL::to('/laporan')}}"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Pembukuan</a></li>
            <li><a href="{{ URL::to('/order')}}"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Order <span class="badge" style="background-color: #d9534f;">1</span></a></li>
            @elseif ($status == '1')
            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> Produk<span class="caret"></span>&nbsp</a>
              <ul class="dropdown-menu">
                <li><a href="{{ URL::to('/washitape')}}"> Washitape</a></li>
                <li><a href="{{ URL::to('/stiker')}}"> Stiker</a></li>
                <li><a href="{{ URL::to('/kartupos')}}"> Kartupos</a></li>
              </ul>
            </li>
            <li><a href="{{ URL::to('/cart')}}"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Cart <span class="badge" style="background-color: #d9534f;">1</span></a></li>
            @endif
            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Hi, {{$nama}}<span class="caret"></span>&nbsp</a>
              <ul class="dropdown-menu">
                <li><a href="{{ URL::to('/profil')}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Ubah Profil</a></li>
                <li><a href="{{ URL::to('/logout')}}"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Keluar</a></li>
              </ul>
            </li>
          </ul>
        </div>
  </div>
</nav>

<div class="col-md-12" align="center" style="opacity:0.15; position:fixed;" ><img src="{{ URL::to('assets/images/washiindonesia.png')}}"></div>
    