<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div class="panel panel-primary">
            <div class="panel-heading">
                <h2 align="center" class="panel-title">Activate your account / Aktifkan akunmu</h2>
            </div>
            <div class="panel-body">
                <p align="center">Thanks for registering in our online shop. Please follow the link below to activate your account.<br></p>
                <p align="center">Terima kasih telah mendaftar di toko online kami. Silahkan ikuti tautan di bawah ini untuk mengaktifkan akun kamu.<br></p>
                <h3 align="center"><a href="{{ URL::to('register/verify/'.$key) }}">{{ URL::to('register/verify/'.$key) }}</a></h3>
            </div>
        </div>
	</body>
</html>