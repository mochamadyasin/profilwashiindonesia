@extends('layouts.general')
@section('content')

    <!-- Three columns of text below the carousel -->
    <div class="container marketing">
      <h2 align="center">TERBARU</h2>
      <p align="center"><a align="center" href="#">Masuk</a> untuk melihat lebih banyak atau membeli barang</p>
      <div class="row">
        <div align="center" class="col-sm-3">
          <img class="img-thumbnail img-responsive" src="assets/images/Slide3.jpg" alt="Terbaru #1" width="140" height="140">
          <h3>IDR 10.000,00</h3>
          <p>WAS1234</p>
        </div>
        <div align="center" class="col-sm-3">
          <img class="img-thumbnail img-responsive" src="assets/images/Slide3.jpg" alt="Terbaru #2" width="140" height="140">
          <h3>IDR 10.000,00</h3>
          <p>WAS1234</p>
        </div>
        <div align="center" class="col-sm-3">
          <img class="img-thumbnail img-responsive" src="assets/images/Slide3.jpg" alt="Terbaru #3" width="140" height="140">
          <h3>IDR 10.000,00</h3>
          <p>WAS1234</p>
        </div>
        <div align="center" class="col-sm-3">
          <img class="img-thumbnail img-responsive" src="assets/images/Slide3.jpg" alt="Terbaru #4" width="140" height="140">
          <h3>IDR 10.000,00</h3>
          <p>WAS1234</p>
        </div>
      </div>
    </div>

    <hr class="featurette-divider">

    <div class="container marketing">
      <h2 align="center">TERLARIS</h2>
      <p align="center"><a align="center" href="#">Masuk</a> untuk melihat lebih banyak atau membeli barang</p>
      <div class="row">
        <div align="center" class="col-sm-3">
          <img class="img-thumbnail img-responsive" src="assets/images/Slide2.jpg" alt="Terlaris #1" width="140" height="140">
          <h3>IDR 10.000,00</h3>
          <p>WAS1204</p>
        </div>
        <div align="center" class="col-sm-3">
          <img class="img-thumbnail img-responsive" src="assets/images/Slide2.jpg" alt="Terlaris #2" width="140" height="140">
          <h3>IDR 10.000,00</h3>
          <p>WAS1204</p>
        </div>
        <div align="center" class="col-sm-3">
          <img class="img-thumbnail img-responsive" src="assets/images/Slide2.jpg" alt="Terlaris #3" width="140" height="140">
          <h3>IDR 10.000,00</h3>
          <p>WAS1204</p>
        </div>
        <div align="center" class="col-sm-3">
          <img class="img-thumbnail img-responsive" src="assets/images/Slide2.jpg" alt="Terlaris #4" width="140" height="140">
          <h3>IDR 10.000,00</h3>
          <p>WAS1204</p>
        </div>
      </div>
    </div>
  
@stop