@extends('layouts.general')
@section('content')

<div class="container">
  <div class="row">
    <div class="panel">
      <div class="panel-body">
        <div class='col-md-7'>
          <div class="row form-group">
            <div class='col-md-3'><p>Dikirim Dari :</p></div>
            <div class='col-md-5'><p>Surabaya - Jawa Timur</p></div>
          </div>
          <div class="row form-group">
            <div class='col-md-3'><p>Provinsi Tujuan :</p></div>
            <div class='col-md-5'>
              <select id="provinsi" name='provinsi' class='form-control'>
                <option value="0">--Pilih Provinsi Tujuan--</option>
                @for($i=0;$i < count($data['rajaongkir']['results']);$i++)
                <option value="{{$data['rajaongkir']['results'][$i]['province_id']}}">{{$data['rajaongkir']['results'][$i]['province']}}</option>
                @endfor
              </select>
            </div>
          </div>
          <div class="row form-group">
            <div class='col-md-3'><p>Kota Tujuan :</p></div>
            <div class='col-md-5'>
              <select id="kota" name='kota' class='form-control'></select>
            </div>
          </div>
          <div class="row form-group">
            <div class='col-md-3'><p>Berat(gram) :</p></div>
            <div class='col-md-5'>
              <input class="form-control" id="berat" name='berat' type="number" required>
            </div>
          </div>
          <div class="row form-group">
            <div class='col-md-3'><p>Kurir :</p></div>
            <div class='col-md-5'>
              <select id="kurir" name='kurir' class='form-control'>
                <option value="jne">JNE</option>
                <option value="pos">POS</option>
              </select>
            </div>
          </div>
          <div class="row form-group">
            <div class='col-md-3'>
              <button id="tombol" type="button" class="btn btn-primary export-button">Perbarui</button>
            </div>
          </div>
        </div>
        <div class='col-md-4'>
          <div id="result">
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready( function() {

    $('#provinsi').on('change',function() {
      var addr = "{{ url('cart/ongkir/') }}";
      $.get(addr+"/"+$('#provinsi').val(),{}, function(data) {
        $('#kota').html(data);
      });
    });

    $('#tombol').on('click',function() {
      $('#result').empty();
      var addr = "{{ url('cart/ongkir/444') }}";
      $.post(addr+"/"+$('#kota').val()+"/"+$('#berat').val()+"/"+$('#kurir').val(),{}, 
      function(data) {
        $('#result').html(data);
      });
    });
  });
</script>

@stop