@extends('layouts.general')
@section('content')

<div class="container marketing">
    <h3 align="center">GUDANG</h3>
    @if (Session::has('message'))
    <div class="alert alert-{{ Session::get('message_type') }} alert-dismissable">
        <i class="fa fa-{{ Session::get('message_type') }}"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        {{ Session::get('message') }}
    </div>
    @endif
    <div class="row">
    	@foreach($barang as $item)
        <div align="center" class="col-sm-3">
            <div class="thumbnail">
	        	<input type="image" class="img-thumbnail img-responsive" src="{{$item->url_pic}}" alt="Gambar Barang" onclick="popupimage('{{$item->url_pic}}')">
	        	<div class="caption">
                    <button name="del" id="del" type="submit" class="btn btn-danger btn-xs">
    	        		<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
    	        	</button>
    	        	<button name="edit" id="edit" type="submit" class="btn btn-info btn-xs">
    	        		<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
    	        	</button>
    	        	<button name="add" id="add" type="submit" class="btn btn-success btn-xs">
    	        		<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
    	        	</button>
                	</br>{{$item->kode}} / {{$item->stok}} 
    	        	</br>IDR {{$item->harga_beli}} / {{$item->harga_jual}}
    	        	</br>{{$item->size}} / {{$item->weight}} gram
                </div>
            </div>
        </div>
        @endforeach
        <div align="center" class="col-sm-3">
        	<input type="image" class="img-thumbnail img-responsive" src="assets/images/add.png" alt="Tambah Barang" onclick="addbarang()">
        	<p align="center">Barang Baru</p>
        </div>
    </div>
</div>

<div id="popupimage" align="center" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content" align="center">
            <img id="imagepopup" class="img-thumbnail img-responsive" alt="Gambar Barang">
        </div>
    </div>
</div>

<div id="addbarang" align="center" class="modal fade">
    <div class="modal-dialog">
        <form action="{{ url('gudang') }}" method="post" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3>TAMBAH BARANG</h3>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class='col-md-6'>
                            <input id="kode" name="kode" type="text" class="form-control uppercase" placeholder="Kode Barang" maxlength="6" required>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-primary btn-file">
                                        Browse&hellip; <input id="gambar" name="gambar" type="file" accept="image/*" required>
                                    </span>
                                </span>
                                <input type="text" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class='col-md-4'>
                            <input id="distributor" name="distributor" type="text" class="form-control" placeholder="Distributor" required>
                        </div>
                        <div class='col-md-4'>
                            <input id="brand" name="brand" type="text" class="form-control" placeholder="Brand / Merek" required>
                        </div>
                        <div class='col-md-4'>
                            <select id="jenis" name='jenis' class='form-control'>
                                <option value="1">Washitape</option>
                                <option value="2">Stiker</option>
                                <option value="3">Kartupos</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class='col-md-4'>
                            <input id="kuantitas" name="kuantitas" type="number" class="form-control" placeholder="Kuantitas" required>
                        </div>
                        <div class='col-md-4'>
                            <input id="ukuran" name="ukuran" type="text" class="form-control" placeholder="Ukuran" required>
                        </div>
                        <div class='col-md-4'>
                            <input id="berat" name="berat" type="number" class="form-control" placeholder="Berat (gram)" required>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class='col-md-6'>
                            <input id="hargabeli" name="hargabeli" type="number" class="form-control" placeholder="Harga Beli (@ Rp)" required>
                        </div>
                        <div class='col-md-6'>
                            <input id="hargajual" name="hargajual" type="number" class="form-control" placeholder="Harga Jual (@ Rp)" step="500" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-info" type="submit" name="submit">Tambah</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div id="addstok" align="center" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
        </div>
    </div>
</div>

<div id="edit" align="center" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
        </div>
    </div>
</div>

<div id="confirmdel" align="center" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            
        </div>
    </div>
</div>

<script>

    $(document).ready( function() {

        $(document).on('change', '.btn-file :file', function() {
            var input = $(this),
                numFiles = input.get(0).files ? input.get(0).files.length : 1,
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
        });

        $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' File dipilih' : label;
            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }
        });
    });

    function popupimage(val){
        $('#imagepopup').attr('src', val);
        $('#popupimage').modal('show');
    }

    function addbarang(){
        $('#addbarang').modal('show');
    }
</script>

@stop