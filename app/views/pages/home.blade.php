<!DOCTYPE html>
<html lang="en" style="height: 100%">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" href="{{ URL::to('assets/images/icon.png') }}"/>
    <link rel="bookmark" href="{{ URL::to('assets/images/icon.png') }}"/>

    <title>Washiindonesia Online Shop</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Load jQuery -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>

    <!-- Load ScrollTo -->
    <script src="http://flesler-plugins.googlecode.com/files/jquery.scrollTo-1.4.2-min.js"></script>

    <!-- Load LocalScroll -->
    <script src="http://flesler-plugins.googlecode.com/files/jquery.localscroll-1.2.7-min.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="assets/css/theme.css" rel="stylesheet">
  </head>
  <body style="height: 100%; overflow-x: hidden; background-color: rgb(241, 242, 246);">
    <!-- Header -->
    <nav style="background-color:white" class="navbar navbar-default navbar-static-top" role="navigation">
      <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" >
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="{{ URL::to('') }}"><img src="{{ URL::to('assets/images/Logo.png')}}" height="27px" alt="washiindonesia"></a>
            </div>
            <div id="navbar-links" class="collapse navbar-collapse">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="#">{{ trans('menu.beranda') }}</a></li>
                <li><a href="#washi">Washitape</a></li>
                <li><a href="#stiker">{{ trans('menu.stiker') }}</a></li>
                <li><a href="#kartu">{{ trans('menu.kartu') }}</a></li>
                <li><a href="#kontak">{{ trans('menu.belanja') }}</a></li>
                <li><a href="#kontak">{{ trans('menu.kontak') }}</a></li>
                <li>
                  <?php if (App::getLocale()=='id') {?>
                    <a class="navbar-brand" href="{{ URL::to('english') }}">
                      <img src="{{ URL::to('assets/images/id.png')}}" height="20px" alt="Bahasa">
                    </a>
                  <?php } else if (App::getLocale()=='en') {?>
                    <a class="navbar-brand" href="{{ URL::to('bahasa') }}">
                      <img src="{{ URL::to('assets/images/en.png')}}" height="20px" alt="English">
                    </a>
                  <?php } ?>
                </li>
              </ul>
            </div>
      </div>
    </nav>

    <!-- End Header -->

    <!-- Carousel
    ================================================== -->
    <div class="col-md-12" align="center" style="opacity:0.15; position:fixed;" ><img src="{{ URL::to('assets/images/washiindonesia.png')}}"></div>
    <div class="container">
      <div class="row featurette">
        <div class="col-md-6">
          <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
              <li data-target="#myCarousel" data-slide-to="2"></li>
              <li data-target="#myCarousel" data-slide-to="3"></li>
              <li data-target="#myCarousel" data-slide-to="4"></li>
            </ol>
            <div class="carousel-inner" role="listbox" align="center">
              <div class="item active">
                <img class="first-slide" src="assets/images/Slide2.jpg" alt="First slide">
                <div class="container">
                  <div class="carousel-caption">
                  </div>
                </div>
              </div>
              <div class="item">
                <img class="second-slide" src="assets/images/Slide3.jpg" alt="Second slide">
                <div class="container">
                  <div class="carousel-caption">
                  </div>
                </div>
              </div>
              <div class="item">
                <img class="third-slide" src="assets/images/Slide4.jpg" alt="Third slide">
                <div class="container">
                  <div class="carousel-caption">
                  </div>
                </div>
              </div>
              <div class="item">
                <img class="fourth-slide" src="assets/images/Slide5.png" alt="Fourth slide">
                <div class="container">
                  <div class="carousel-caption">
                  </div>
                </div>
              </div>
              <div class="item">
                <img class="fifth-slide" src="assets/images/Slide6.png" alt="Fifth slide">
                <div class="container">
                  <div class="carousel-caption">
                  </div>
                </div>
              </div>
            </div>
            <a class="left carousel-control" style="background-opacity:0" href="#myCarousel" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div><!-- /.carousel -->
        </div>
        <div class="col-md-6">
          @if (Session::has('message'))
          <div class="alert alert-{{ Session::get('message_type') }} alert-dismissable">
            <i class="fa fa-{{ Session::get('message_type') }}"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('message') }}
          </div>
          @endif
          <form action="{{ url('login') }}" method="get">
            <div class="form-group">
              <h3>{{trans('home.login')}}</h3>
            </div>
            <div class="form-group form-inline">
              <input class="form-control" id="email-login" name="email-login" placeholder="{{trans('home.loginuser')}}" type="email" required>
              <input class="form-control" id="password-login" name="password-login" placeholder="{{trans('home.loginpass')}}" type="password" required>
              <button class="btn btn-info" type="submit">{{trans('home.loginpush')}}</button>
              <p style="color:#3BAFDA;font-size:11px">{{trans('home.loginnote')}}</p>
            </div>
          </form>
          <form action="{{ url('register') }}" method="post">
            <div class="form-group">
              <h3>{{trans('home.reg')}}</h3>
            </div>
            <div class="form-group">
              <input class="form-control" id="name" name="name" placeholder="{{trans('home.regname')}}" type="text"  required>
            </div>
            <div class="form-group">
              <input class="form-control" id="email" name="email" placeholder="{{trans('home.reguser')}}" type="email" required>
            </div>
            <div class="form-group">
              <input class="form-control" id="password" name="password" placeholder="{{trans('home.regpass')}}" type="password" pattern=".{8,}" required>
            </div>
            <div class="form-group">
              <input class="form-control" id="password-again" placeholder="{{trans('home.regpass2')}}" type="password" onblur="checkPassword()" pattern=".{8,}" required>
            </div>
            <div class="form-group">
              <button style="width:100%" class="btn btn-info" type="submit">{{trans('home.regpush')}}</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <!-- <div class="container marketing"> -->
    <hr class="featurette-divider">

    <!-- Three columns of text below the carousel -->
    <div class="container marketing">
      <h2 align="center">TERBARU</h2>
      <p align="center"><a align="center" href="#">Masuk</a> untuk melihat lebih banyak atau membeli barang</p>
      <div class="row">
        <div align="center" class="col-sm-3">
          <img class="img-thumbnail img-responsive" src="assets/images/Slide3.jpg" alt="Terbaru #1" >
          <h3>IDR 10.000,00</h3>
          <p>WAS1234</p>
        </div>
        <div align="center" class="col-sm-3">
          <img class="img-thumbnail img-responsive" src="assets/images/Slide3.jpg" alt="Terbaru #2" >
          <h3>IDR 10.000,00</h3>
          <p>WAS1234</p>
        </div>
        <div align="center" class="col-sm-3">
          <img class="img-thumbnail img-responsive" src="assets/images/Slide3.jpg" alt="Terbaru #3" >
          <h3>IDR 10.000,00</h3>
          <p>WAS1234</p>
        </div>
        <div align="center" class="col-sm-3">
          <img class="img-thumbnail img-responsive" src="assets/images/Slide3.jpg" alt="Terbaru #4" >
          <h3>IDR 10.000,00</h3>
          <p>WAS1234</p>
        </div>
      </div>
    </div>

    <hr class="featurette-divider">

    <div class="container marketing">
      <h2 align="center">TERLARIS</h2>
      <p align="center"><a align="center" href="#">Masuk</a> untuk melihat lebih banyak atau membeli barang</p>
      <div class="row">
        <div align="center" class="col-sm-3">
          <img class="img-thumbnail img-responsive" src="assets/images/Slide2.jpg" alt="Terlaris #1" >
          <h3>IDR 10.000,00</h3>
          <p>WAS1204</p>
        </div>
        <div align="center" class="col-sm-3">
          <img class="img-thumbnail img-responsive" src="assets/images/Slide2.jpg" alt="Terlaris #2" >
          <h3>IDR 10.000,00</h3>
          <p>WAS1204</p>
        </div>
        <div align="center" class="col-sm-3">
          <img class="img-thumbnail img-responsive" src="assets/images/Slide2.jpg" alt="Terlaris #3" >
          <h3>IDR 10.000,00</h3>
          <p>WAS1204</p>
        </div>
        <div align="center" class="col-sm-3">
          <img class="img-thumbnail img-responsive" src="assets/images/Slide2.jpg" alt="Terlaris #4" >
          <h3>IDR 10.000,00</h3>
          <p>WAS1204</p>
        </div>
      </div>
    </div>

    <!-- START THE FEATURETTES -->
    <div class="container">

      <hr class="featurette-divider">

      <div id="washi" class="row featurette">
        <div class="col-sm-8">
          <h2 class="featurette-heading">{{trans('home.washi1')}}<span class="text-muted">{{trans('home.washi2')}}</span></h2>
          <p class="lead">{{trans('home.washi3')}} &middot; <a href="#">{{trans('menu.kembali')}}</a></p>
        </div>
        <div class="col-sm-4">
          <img class="featurette-image img-circle img-responsive center-block" src="assets/images/Product1.jpg" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider">

      <div id="stiker" class="row featurette">
        <div class="col-sm-8 col-sm-push-4">
          <h2 class="featurette-heading">{{trans('home.stiker1')}}<span class="text-muted">{{trans('home.stiker2')}}</span></h2>
          <p class="lead">{{trans('home.stiker3')}} &middot; <a href="#">{{trans('menu.kembali')}}</a></p>
        </div>
        <div class="col-sm-4 col-sm-pull-8">
          <img class="featurette-image img-circle img-responsive center-block" src="assets/images/Product2.jpg" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider">

      <div id="kartu" class="row featurette">
        <div class="col-sm-8">
          <h2 class="featurette-heading">{{trans('home.kartu1')}}<span class="text-muted">{{trans('home.kartu2')}}</span></h2>
          <p class="lead">{{trans('home.kartu3')}} &middot; <a href="#">{{trans('menu.kembali')}}</a></p>
        </div>
        <div class="col-sm-4">
          <img class="featurette-image img-circle img-responsive center-block" src="assets/images/Product3.jpg" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider">

      <div id="kontak" class="row featurette">
        <div class="col-sm-8 col-sm-push-4">
          <h2 class="featurette-heading">{{trans('home.kontak1')}}<span class="text-muted">{{trans('home.kontak2')}}</span></h2>
          <p class="lead">{{trans('home.kontak3')}} &middot; <a href="#">{{trans('menu.kembali')}}</a></p>
        </div>
        <div class="col-sm-4 col-sm-pull-8">
          <img class="featurette-image img-thumbnail img-responsive center-block" src="assets/images/Slide5.png" alt="Generic placeholder image">
        </div>
      </div>

    </div>

    <script>
      $(document).ready(function()
      {
        // Scroll the whole document
        $('#navbar-links').localScroll({
          target:'body'
        });
      });

      function checkPassword(){
        var pass1 = document.getElementById("password").value;
        var pass2 = document.getElementById("password-again").value;
        
        if(pass2!=pass1){
          document.getElementById("password-again").value = '';
        }
      }
    </script>

    <!-- Footer -->
    <footer class="footer">
      <div class="container"> 
        <p class="pull-right">{{trans('menu.dibuat')}} <a href="#">BrillianISay</a></p>
        <p>&copy; {{date("Y")}} Washiindonesia. &middot; <a href="#">{{trans('menu.privasi')}}</a> &middot; <a href="#">{{trans('menu.kondisi')}}</a> &middot; <a href="#">{{trans('menu.kembali')}}</a> </p>
      </div>
    </footer>
      <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="assets/bootstrap/dist/js/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/bootstrap/dist/js/ie10-viewport-bug-workaround.js"></script>

    <!-- End Footer -->
  </body>
</html>



