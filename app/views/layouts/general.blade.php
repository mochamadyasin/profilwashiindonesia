<!DOCTYPE html>
<html lang="en" style="height: 100%">
  <head>
    @include('includes.header')
  </head>
  <body style="height: 100%; overflow-x: hidden; background-color: rgb(241, 242, 246);">
    <!-- Header -->
    @include('includes.navbar')
    <!-- End Header -->
    @yield('content')
    <!-- Footer -->
    @include('includes.footer')
    <!-- End Footer -->
  </body>
</html>