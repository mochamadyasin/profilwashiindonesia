<?php

class Users extends BaseModel{

	public function getUserByEmail($email) 
	{
		$result = DB::table('users')->where('email',$email)->first();

		return $result;
	}

	public function insertUser($name,$email,$password,$key)
	{
		DB::table('users')->insert(array('name' => $name, 'email' => $email, 'password' => $password, 'key' => $key, 'created_at' => date("Y-m-d H:i:sa")));
	}

	public function getUserByKey($key)
	{
		$result = DB::table('users')->where('key',$key)->first();

		return $result;
	}

	public function updateUserByKey($key)
	{
		DB::table('users')->where('key',$key)->update(array('is_confirmed' => '1', 'updated_at' => date("Y-m-d h:i:sa")));
	}

}

?>