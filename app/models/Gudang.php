<?php

class Gudang extends BaseModel{

	use SoftDeletingTrait;
	protected $table = 'tmst_barang';
	protected $primaryKey = 'kode';
	public $Stamps = true;
	protected $softDelete = true;
	protected $fillable = array(
            'kode_jenis_barang',
            'brand',
            'deskripsi',
            'harga_jual',
            'harga_beli',
            'stok',
            'url_pic',
            'size',
            'weight',
	);
}

?>