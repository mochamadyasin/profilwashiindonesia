<?php

class Pembelian extends BaseModel{

	use SoftDeletingTrait;
	protected $table = 'tran_pembelian_barang';
	protected $primaryKey = 'kode';
	public $Stamps = true;
	protected $softDelete = true;
	protected $fillable = array(
            'kode_barang',
            'harga_beli',
            'kuantitas',
            'distributor',
	);
}

?>