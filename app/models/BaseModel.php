<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class BaseModel extends Eloquent{

    /**
     * The default date format in SQL Server 2005
     * @var string
     */
    const SQLSRV_DATE_FORMAT = 'M j Y H:i:s:000A';

    /**
     * The date format.
     * @var string
     */
    protected $dateFormat;

    public function __construct()
    {
        parent::__construct();
        $this->dateFormat = parent::getDateFormat();
    }

    public function getDateFormat()
    {
        return $this->dateFormat;
    }

    public function setDateFormat($value)
    {
        $this->dateFormat = $value;
    }

    /**
     * Override default date converter
     * @param  mixed $value date value
     * @return mixed        date
     */
    protected function asDateTime($value)
    {
        if ($this->getConnection()->getDriverName() != 'mysql') 
        {
            $this->setDateFormat(static::SQLSRV_DATE_FORMAT);
        }

        return parent::asDateTime($value);
    }

}