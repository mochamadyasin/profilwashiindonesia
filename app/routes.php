<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	$status = Session::get('status');
	if($status == '0') return View::make('pages.admin');
	else if($status == '1') return View::make('pages.user');
	else return View::make('pages.home');
});

Route::get('english', function()
{
	App::setLocale('en');

	return View::make('pages.home');
});

Route::get('bahasa', function()
{
	App::setLocale('id');

	return View::make('pages.home');
});

Route::post('register', 'UsersController@register');
Route::get('register/verify/{key}', 'UsersController@verify');
Route::get('login', 'UsersController@login');

Route::get('cart', 'CartController@index');
Route::get('cart/ongkir/{provinsi}', 'CartController@getCity');
Route::post('cart/ongkir/444/{kota}/{berat}/{kurir}', 'CartController@getOngkir');

Route::get('gudang', 'GudangController@index');
Route::post('gudang', 'GudangController@addBarang');

Route::get('/washitape', function()
{
	$status = Session::get('status');
	if($status != '' || $status != null) return View::make('pages.washitape');
	else return View::make('pages.home');
});

Route::get('/logout', function()
{
	Session::flush();
	return View::make('pages.home');
});
