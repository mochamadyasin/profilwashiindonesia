<?php

return array(

	'beranda'	=> 'Home',
	'stiker'	=> 'Sticker',
	'kartu'		=> 'Postcard',
	'belanja'	=> 'Shopping',
	'kontak'	=> 'Contact Us',
	'privasi'	=> 'Privacy',
	'kondisi'	=> 'Terms',
	'kembali'	=> 'Back to top',
	'dibuat'	=> 'Developed by',

);
