<?php

return array(

	'login'		=> 'LOGIN',
	'loginuser'	=> 'Email',
	'loginpass'	=> 'Password',
	'loginpush' => 'Login',
	'loginnote'	=> 'Dont have account? fill the form below!',

	'reg'		=> 'CREATE FREE ACCOUNT',
	'regname'	=> 'Your Name',
	'reguser'	=> 'Email',
	'regpass'	=> 'Password (minimum 8 character)',
	'regpass2'	=> 'Retype Password',
	'regpush'	=> 'Register',

	'regdone'	=> 'Thanks for registering! Please check your email to activate your account.',
	'regexist'	=> 'Your email is already exist! Please login or register with another email.',
	'regactive'	=> 'Congratulation! your account is activated, Loging in and Happy Shopping.',
	'regnot'	=> 'Your account not being created. Please register again.',

	'washi1'	=> 'Our main item | ',
	'washi2'	=> 'Washitape.',
	'washi3'	=> 'We are selling many kind of washitapes, from unbranded to branded item. Many of our washitape are made in Japanese.',
	'stiker1'	=> 'Paste, paste & paste | ',
	'stiker2'	=> 'Sticker.',
	'stiker3'	=> 'Cutest stickers are ready only for you. We are selling stickers from unbranded to branded item. Our stickers also made in Japanese.',
	'kartu1'	=> 'Tell your feeling | ',
	'kartu2'	=> 'Postcard.',
	'kartu3'	=> 'With our cutest postcard, your feeling will conveyed more deep to your people.',
	'kontak1'	=> 'You can order here | ',
	'kontak2'	=> 'Contact us.',
	'kontak3'	=> 'Our online shop is under construction, so you can buy our item that do you want using our contact. Happy shopping :)',

);
