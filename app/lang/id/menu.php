<?php

return array(

	'beranda'	=> 'Beranda',
	'stiker'	=> 'Stiker',
	'kartu'		=> 'Kartupos',
	'belanja'	=> 'Belanja',
	'kontak'	=> 'Kontak',
	'privasi'	=> 'Privasi',
	'kondisi'	=> 'Kondisi',
	'kembali'	=> 'Kembali ke atas',
	'dibuat'	=> 'Dibuat oleh',

);
