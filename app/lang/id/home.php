<?php

return array(

	'login'		=> 'MASUK',
	'loginuser'	=> 'Email',
	'loginpass'	=> 'Kata Sandi',
	'loginpush' => 'Masuk',
	'loginnote'	=> 'Belum punya akun? daftar aja di bawah ini!',

	'reg'		=> 'BUAT AKUN GRATIS',
	'regname'	=> 'Nama Kamu',
	'reguser'	=> 'Email',
	'regpass'	=> 'Kata Sandi (minimal 8 karakter)',
	'regpass2'	=> 'Ulang Kata Sandi',
	'regpush'	=> 'Mendaftar',

	'regdone'	=> 'Terima kasih telah mendaftar! Silahkan cek email untuk mengaktifkan akunmu.',
	'regexist'	=> 'Email kamu sudah terdaftar! Silahkan masuk atau buat akun baru.',
	'regactive'	=> 'Selamat! Akunmu sudah aktif, Masuk dan selamat berbelanja.',
	'regnot'	=> 'Akunmu belum terdaftar. Silahkan mendaftarkan akunmu.',

	'washi1'	=> 'Penjualan utama kami | ',
	'washi2'	=> 'Washitape.',
	'washi3'	=> 'Kami menjual berbagai macam washitape, dari yang tidak bermerek sampai bermerek. Kebanyakan washitape yang kami jual adalah impor dari Jepang.',
	'stiker1'	=> 'Tempel, tempel dan tempel | ',
	'stiker2'	=> 'Stiker.',
	'stiker3'	=> 'Stiker yang lucu dan menarik siap kami sediakan hanya untuk kamu. Kami menjual stiker yang tidak bermerek sampai yang bermerek. Kami juga impor dari Jepang untuk stiker-stiker yang kami jual.',
	'kartu1'	=> 'Sampaikan perasaanmu | ',
	'kartu2'	=> 'Kartu Pos.',
	'kartu3'	=> 'Dengan kartu pos yang lucu dan menarik, dijamin perasaan kamu akan tersampaikan lebih mendalam kepada orang yang kamu tuju.',
	'kontak1'	=> 'Kamu bisa memesan disini | ',
	'kontak2'	=> 'Hubungi kami ya.',
	'kontak3'	=> 'Sambil menunggu website online shop kami, kamu bisa beli item yang kamu inginkan lewat kontak kami ya. Selamat berbelanja :)',

);
