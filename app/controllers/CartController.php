<?php

class CartController extends BaseController {

  public function index()
  {
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "http://api.rajaongkir.com/starter/province",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "key: 15377d3d86076ebe0b48756603accab5"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
    $data = json_decode($response,true);

    return View::make('pages.cart',compact('data'))->render();
  }

  public function getCity($provinsi)
  {
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "http://api.rajaongkir.com/starter/city?province=$provinsi",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "key: 15377d3d86076ebe0b48756603accab5"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
    $data = json_decode($response,true);
    
    for ($i=0; $i < count($data['rajaongkir']['results']); $i++) { 
      echo "<option value=".$data['rajaongkir']['results'][$i]['city_id'].">".$data['rajaongkir']['results'][$i]['type']." ".$data['rajaongkir']['results'][$i]['city_name']."</option>";
    }
    
  }

  public function getOngkir($kota,$berat,$kurir)
  {
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "http://api.rajaongkir.com/starter/cost",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "origin=444&destination=$kota&weight=$berat&courier=$kurir",
      CURLOPT_HTTPHEADER => array(
        "content-type: application/x-www-form-urlencoded",
        "key: 15377d3d86076ebe0b48756603accab5"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
    $data = json_decode($response,true);
    
    echo "<p value=".$data['rajaongkir']['results'][0]['name'].">".$data['rajaongkir']['results'][0]['name']."</p>";
    for ($i=0; $i < count($data['rajaongkir']['results'][0]['costs']); $i++) {
      echo "<div class='radio'><label><input type='radio' name='optradio'>".$data['rajaongkir']['results'][0]['costs'][$i]['service']." : ";
      echo "Rp ".$data['rajaongkir']['results'][0]['costs'][$i]['cost'][0]['value']." | ";
      echo "Pengiriman ".$data['rajaongkir']['results'][0]['costs'][$i]['cost'][0]['etd']." hari</label></div>";
    }

  }

}

?>