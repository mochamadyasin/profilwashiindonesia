<?php

class UsersController extends BaseController {

	public function login(){
		$email = Input::get('email-login');
		$password = md5(Input::get('password-login'));
		$users = new Users;
		$login = -3;

		$user = $users->getUserByEmail($email);
		if($user)
		{
			if($user->is_confirmed == 1)
			{
				if($user->password == $password)
				{
					if($user->status == 0) $login = 0;
					else $login = 1;
					Session::put('nama', $user->name); 
					Session::put('status', $user->status); 
				}
				else 
				{
					$login = -1;
				}
			}
			else
			{
				$login = -2;
			}
		}

		switch ($login) {
			case 1:
				return Redirect::to('/');
				break;
			case 0:
				return Redirect::to('/');
				break;
			case -1:
				return Redirect::to('/')->with('message','Password kamu salah.')->with('message_type','warning');
				break;
			case -2:
				return Redirect::to('/')->with('message','Akun kamu belum aktif, silahkan ikuti link yang kami kirim ke email kamu.')->with('message_type','warning');
				break;
			case -3:
				return Redirect::to('/')->with('message','Akun kamu belum terdaftar, silahkan daftarkan akun kamu.')->with('message_type','warning');
				break;
		}
	}

	public function register()
	{
		$name = Input::get('name');
		$email = Input::get('email');
		$password = md5(Input::get('password'));
		$key = str_random(30);
		$users = new Users;

		$cek = $users->getUserByEmail($email);
		
		if(!$cek)
		{
			$users->insertUser($name,$email,$password,$key);

			Mail::send('emails.verify', array('key' => $key), function($message){
				$message->from('admin@washiindonesia.xyz','Washiindonesia');
				$message->subject('Account Activation / Aktivasi Akun');
				$message->to(Input::get('email'),Input::get('name'));
			});

			return Redirect::to('/')->with('message',trans('home.regdone'))->with('message_type','info');
		}
		else
		{
			return Redirect::to('/')->with('message',trans('home.regexist'))->with('message_type','warning');
		}
	}

	public function verify($key){
		$users = new Users;

		$cek = $users->getUserByKey($key);

		if($cek)
		{
			$users->updateUserByKey($key);

			return Redirect::to('/')->with('message',trans('home.regactive'))->with('message_type','info');
		}
		else
		{
			return Redirect::to('/')->with('message',trans('home.regnot'))->with('message_type','warning');
		}
	}
}

?>