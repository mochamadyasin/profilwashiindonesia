<?php

class GudangController extends BaseController {
	
	public function index()
	{
		$gudang = new Gudang;
		$barang = $gudang->get();

		$status = Session::get('status');
		if($status == '0') return View::make('pages.gudang',compact('barang'))->render();
		else if($status == '1') return View::make('pages.user');
		else return View::make('pages.home');
	}

	public function addBarang()
	{
		$kode = Input::get('kode');
		$target_dir = "files/images/";
		$uploadOk = 1;
		$message = '';
		$namaFile = basename($_FILES["gambar"]["name"]);
		$imageFileType = pathinfo($namaFile,PATHINFO_EXTENSION);
		$target_file = $target_dir.$kode.'.'.$imageFileType;
		// Check if image file is a actual image or fake image
		if(isset($_POST["submit"])) {
		    $check = getimagesize($_FILES["gambar"]["tmp_name"]);
		    if($check !== false) {
		        $uploadOk = 1;
		    } else {
		        $uploadOk = 0;
		    }
		}
		// Check if file already exists
		if (file_exists($target_file)) {
		    $message .= "Kode barang ".$kode." sudah ada. Silahkan gunakan kode yang lain.";
		    $uploadOk = 0;
		    return Redirect::to('gudang')->with('message', $message)->with('message_type', 'warning');
		}
		// Check file size
		if ($_FILES["gambar"]["size"] > 100000) {
		    $message .= "Ukuran file terlalu besar, pastikan file kurang dari 100KB. ";
		    $uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
		    $message .= "Ekstensi file harus JPG, JPEG, PNG atau GIF. ";
		    $uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		    $message .= "File tidak terunggah, silahkan ikuti ketentuan file.";
		    return Redirect::to('gudang')->with('message', $message)->with('message_type', 'warning');
		// if everything is ok, try to upload file
		} else {
		    if (move_uploaded_file($_FILES["gambar"]["tmp_name"], $target_file)) {
		        $message .= "File telah berhasil diunggah. ";
		    } else {
		        $message .= "Ada kesalahan saat unggah file. ";
			    return Redirect::to('gudang')->with('message', $message)->with('message_type', 'danger');
		    }
		}

		$gudang = new Gudang;
		$gudang->kode = Input::get('kode');
		$gudang->kode_jenis_barang = Input::get('jenis');
		$gudang->brand = Input::get('brand');
		$gudang->harga_jual = Input::get('hargajual');
		$gudang->harga_beli = Input::get('hargabeli');
		$gudang->stok = Input::get('kuantitas');
		$gudang->url_pic = $target_file;
		$gudang->size = Input::get('ukuran');
		$gudang->weight = Input::get('berat');
		$gudang->save();

		$pembelian = new Pembelian;
		$pembelian->kode_barang = Input::get('kode');
		$pembelian->harga_beli = Input::get('hargabeli')*Input::get('kuantitas');
		$pembelian->kuantitas = Input::get('kuantitas');
		$pembelian->distributor = Input::get('distributor');
		$pembelian->save();

		$message .= "Tambah barang SUCCESS.";
		return Redirect::to('gudang')->with('message', $message)->with('message_type', 'info');
	}

}

?>